<?php

/**
 * Class View
 * Gestion des vues
 *
 * @author Yonel Becerra
 */

$css        = [];
$head_js    = [];
$footer_js  = [];

class View {

    protected static function init()
    {

    }

    public static function get($view, $datas = [])
    {
        self::init();

        $view = str_replace('.', TLAB_DS, $view);

        extract($datas);

        ob_start();
            include('..'.TLAB_DS.'app'.TLAB_DS.'views'.TLAB_DS.$view.'.tlab.php');
        $return = ob_get_clean();

        $return = self::includes($return);
        $return = self::routes($return);
        $return = self::replaceDatas($return, $datas);

        echo $return;
    }

    // Inclue tous les templates @include('')
    public static function includes($return)
    {
        $matches = [];
        preg_match_all("#@include\(\'(.+)\'\)#", $return, $matches);

        foreach ($matches[0] as $match) {

            $pattern = $match;

            $match = preg_replace( "#^@include\(\'(.+)\'\)$#", '$1', $match );
            $match = str_replace('\\', TLAB_DS, $match);

            ob_start();
                if(file_exists('..'.TLAB_DS.'app'.TLAB_DS.'views'.TLAB_DS.$match.'.tlab.php')) {
                    include('..'.TLAB_DS.'app'.TLAB_DS.'views'.TLAB_DS.$match.'.tlab.php');
                } else {
                    echo '<span style="color: red;">La vue '.$match.'.tlab.php n\'existe pas.</span>';
                }
            $getClean = ob_get_clean();

            $return = str_replace($pattern, $getClean, $return);
        }
        return $return;
    }

    // Inclue tous les templates @routes('')
    public static function routes($return)
    {
        $matches = [];
        preg_match_all("#@route\(\'(.+)\'\)#", $return, $matches);

        foreach ($matches[0] as $match) {

            $pattern = $match;

            $match = preg_replace( "#^@route\(\'(.+)\'\)$#", '$1', $match );

            ob_start();
                Routes::linkTo($match);
            $getClean = ob_get_clean();

            $return = str_replace($pattern, $getClean, $return);
        }
        return $return;
    }

    public static function replaceDatas($return, $datas)
    {

        $matches = [];
        preg_match_all("#{{ .+ }}#", $return, $matches);

        foreach ($matches[0] as $match) {

            // Si c'est une variable simple
            if( preg_match("#^{{ [$](.+) }}$#", $match, $variable) ) {
                // Si la variable a été indiquée lors de l'appel de la vue
                if(array_key_exists($variable[1], $datas)) {
                    $return = str_replace($match, htmlspecialchars($datas[$variable[1]]), $return);
                } else {
                    $return = str_replace($match, '<span style="color: red;" title="Variable non définie pour la vue">'.'$'.$variable[1].'</span>', $return);
                }
            }

        }



        return $return;

    }

    public static function notFound()
    {
        header("HTTP/1.0 404 Not Found");
        die(self::get('pages.404'));
    }

    public static function add_css($slug, $link)
    {
        global $css;
        $css[$slug] = $link;
    }

    public static function add_head_js($slug, $link)
    {
        global $head_js;
        $head_js[$slug] = $link;
    }

    public static function add_footer_js($slug, $link)
    {
        global $footer_js;
        $footer_js[$slug] = $link;
    }

    public static function css($e = true)
    {
        global $css;

        $return = '';

        if(!empty($css)) {
            foreach ($css as $slug => $link) {

                // Si c'est une URL
                if(preg_match('#^http#', $link)) {

                    $return .= '<link rel="stylesheet" name="'.$slug.'" href="'.$link.'">'."\r\n";

                } else if (preg_match('#^\.\.#', $link)) {

                    $return .= '<style>'."\r\n";
                    ob_start();
                        if(file_exists($link)) {
                            include_once($link);
                        }
                    $return .= ob_get_clean()."\r\n";
                    $return .= '</style>'."\r\n";
                }

            }

            if($e)
                echo $return;
        }

        return $return;
    }

    public static function head_js($e = true)
    {
        global $head_js;

        $return = '';

        if(!empty($head_js)) {
            foreach ($head_js as $slug => $link) {

                // Si c'est une URL
                if(preg_match('#^http#', $link)) {
                    $return .= '<script name="' . $slug . '" src="' . $link . '"></script>' . "\r\n";
                } else if (preg_match('#^\.\.#', $link)) {

                    $return .= '<script type="javascript">'."\r\n";
                    ob_start();
                    if(file_exists($link)) {
                        include_once($link);
                    }
                    $return .= ob_get_clean()."\r\n";
                    $return .= '</script>'."\r\n";
                }
            }

            if ($e)
                echo $return;
        }

        return $return;
    }

    public static function footer_js($e = true)
    {
        global $footer_js;

        $return = '';

        if(!empty($footer_js)) {
            foreach ($footer_js as $slug => $link) {
                // Si c'est une URL
                if(preg_match('#^http#', $link)) {
                    $return .= '<script name="' . $slug . '" src="' . $link . '"></script>' . "\r\n";
                } else if (preg_match('#^\.\.#', $link)) {

                    $return .= '<script>'."\r\n";
                    ob_start();
                    if(file_exists($link)) {
                        include_once($link);
                    }
                    $return .= ob_get_clean()."\r\n";
                    $return .= '</script>'."\r\n";
                }
            }

            if ($e)
                echo $return;
        }

        return $return;
    }

    public static function redirect($route)
    {
        header('Location: '.$route);
    }

}