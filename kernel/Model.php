<?php

/**
 * Class Model
 * Classe mère de tous les modèles
 *
 * @author Yonel Becerra
 */

// Connexion PDO à la BDD
$bdd = false;

class Model {

    // Domaines où est située la BDD
    protected $host = 'localhost';

    // Nom de la BDD
    protected $base = 'ecrivains';

    // Nom d'utilisateur
    protected $login = 'root';

    // Mot de passe
    protected $password = '';

    // Nom de la table
    protected $table;

    protected $structure;

    // Constructeur du modèle
    public function __construct()
    {
        // On récupère les informations de connexion à la BDD

        global $bdd;
        // On créée une instance PDO représentant notre connexion en BDD
        if(!$bdd) {
            $bdd = $this->connect();
        }
    }

    // Place une instance PDO représentant notre connexion BDD dans $this->bdd
    protected function connect()
    {

        $dsn = 'mysql:host='.$this->host.';dbname='.$this->base.';charset=utf8';
        $bdd = new PDO($dsn, $this->login, $this->password);

        return $bdd;

    }

    // Permet de récupérer toutes les entrées de la table $this->table
    public function all()
    {
        global $bdd;

        $datas = [];

        $response = $bdd->query('SELECT * FROM '.$this->table.' ORDER BY id DESC');
        $j = 1;

        // S'il n'y a pas de réponse on retourne false
        if($response == false) {
            return false;
        }

        while($donnees = $response->fetch()) {

            // S'il n'y a pas de réponse on retourne false
            if(empty($donnees)) {
                return false;
            }

            foreach ($donnees as $slug => $donnee) {
                if(!is_integer($slug)) {
                    $datas[$j][$slug] = $donnee;
                }
            }
            $j++;
        }

        $response->closeCursor();

        return $datas;
    }

    // Permet d'insérer des données en BDD
    public function insert($data)
    {
        global $bdd;


        $request = 'INSERT INTO '.$this->table.'(';
        $i = 0;

        foreach ($data as $slug => $datum) {
            if($i != 0) {
                $request .= ', ';
            }
            $request .= htmlspecialchars($slug);
            $i++;
        }

        $i = 0;
        $request .= ') VALUES(';

        foreach ($data as $slug => $datum) {
            if($i != 0) {
                $request .= ', ';
            }
            $request .= ':'.htmlspecialchars($slug);
            $i++;
        }

        $request .= ')';

        $req = $bdd->prepare($request);
        $req->execute($data);

    }

    // Permet de mettre à jour des données en BDD
    public function update($id)
    {
        global $bdd;

        foreach ($this->structure as $slug) {
            $data[$slug] = Request::post($slug);
        }

        $request = 'UPDATE '.$this->table.' SET ';
        $i = 0;

        foreach ($data as $slug => $datum) {
            if($i != 0) {
                $request .= ', ';
            }
            $request .= htmlspecialchars($slug).' = :'.$slug;
            $i++;
        }

        $i = 0;
        $request .= ' WHERE id = :id';

        $data['id'] = $id;

        $req = $bdd->prepare($request);
        $req->execute($data);
    }

    // Permet de récupérer des informations
    public function get($id)
    {
        global $bdd;
        $resource = [];

        $response = $bdd->query('SELECT * FROM '.$this->table.' WHERE id='.(int)$id);

        while($donnees = $response->fetch()) {
            foreach ($donnees as $slug => $donnee) {
                if(!is_integer($slug)) {
                    $resource[$slug] = $donnee;
                }
            }
        }

        $response->closeCursor();

        return $resource;
    }

    // Permet de sauvegarder des informations
    public function save($datas = [])
    {
        if(empty($datas)) {
            foreach ($this->structure as $slug) {
                $datas[$slug] = Request::post($slug);
            }
        }

        $this->insert($datas);
    }

    // Permet de mettre à jour des informations
    public function refresh($id)
    {
        foreach ($this->structure as $slug) {
            $datas[$slug] = Request::post($slug);
        }

        $this->update($id, $datas);
    }



    // Permet de supprimer des informations
    public function delete($id)
    {
        global $bdd;

        $bdd->query('DELETE FROM '.$this->table.' WHERE id='.htmlspecialchars($id));


        return true;
    }

    public function getWith($data)
    {
        global $bdd;
        $resources = [];

        $request = 'SELECT * FROM '.$this->table.' WHERE ';

        $i = 0;

        foreach ($data as $slug => $datum) {
            if($i != 0) {
                $request .= ', ';
            }
            $request .= $slug.'=\''.$datum.'\'';
            $i++;
        }


        $response = $bdd->query($request);

        // S'il n'y a pas de réponse on retourne false
        if($response == false) {
            return false;
        }

        $i = 0;

        while($donnees = $response->fetch()) {

            // S'il n'y a pas de réponse on retourne false
            if(empty($donnees)) {
                return false;
            }

            foreach ($donnees as $slug => $donnee) {
                if(!is_integer($slug)) {
                    $resources[$i][$slug] = $donnee;
                }
            }
            $i++;
        }

        $response->closeCursor();

        return $resources;
    }


}