<?php

/**
 * Class Auth
 * Cette classe gère les utilisateurs
 *
 * @author Yonel Becerra
 */

$user = false;

class Auth {

    // Affiche le formulaire d'inscription
    public function register()
    {
        View::get('auth.register');
        return true;
    }

    // Affiche le formulaire de connexion
    public function login()
    {
        View::get('auth.login');
        return true;
    }

    // Affiche le bouton de déconnexion
    public static function logout()
    {
        View::get('auth.logout');
        return true;
    }

    // Enregistre l'utilisateur
    public static function register_func()
    {
        $user = new UserModel();
        $user->save();

        self::login_func([
            'email'     => Request::post('email'),
            'password'  => Request::post('password'),
        ]);

        View::redirect('/');
        die();
    }

    // Connecte l'utilisateur
    public static function login_func()
    {
        if(empty($userDatas)) {
            $userDatas = [
                'email'     => Request::post('email')
            ];
        }

        global $user;

        $user = new UserModel();
        $users = $user->getWith($userDatas);

        if(!empty($users)) {

            if(password_verify(Request::post('password'), $users[0]['password'])) {
                $user = $users[0];

                Globals::addSession('user_id', $user['id']);
                Globals::addSession('user_login', $user['login']);
                Globals::addSession('user_password', $user['password']);
                Globals::addSession('user_email', $user['email']);

                View::redirect('/');
            }
            else {
                View::redirect('/login/');
                die();
            }

        } else {

            View::redirect('/login/');
            die();
        }




        die();
    }

    // Déconnecte l'utilisateur
    public static function logout_func()
    {
        Globals::removeSession('user_id');
        Globals::removeSession('user_login');
        Globals::removeSession('user_password');
        Globals::removeSession('user_email');

        View::redirect('/');
        die();
    }

    // Retourne les informations sur l'utilisateur enregistré
    public static function userInfos()
    {
        $user = new UserModel();
        $users = $user->all();

        if( $users !== false) {
            return $users;
        }

        return false;
    }

    public static function hasUser()
    {
        if( self::userInfos() )
        {
            return true;
        }

        return false;
    }

    public static function check()
    {
        $slugs      = [ 'user_id' => 'id', 'user_login' => 'login', 'user_email' => 'email', 'user_password' => 'password' ];
        $userDatas  = [];


        foreach ($slugs as $alias => $slug) {
            if(empty(Globals::session($alias))) {
                return false;
            }
            $userDatas[$slug] = Globals::session($alias);
        }

        return true;
        die;



        unset($userDatas['id']);
        unset($userDatas['login']);
        unset($userDatas['password']);


        $user = new UserModel();
        $users = $user->getWith($userDatas);




        if(!empty($users)) {
            return true;
        } else {

            return false;
        }


    }

    public function delete_account()
    {
        $user = new UserModel();
        $user_id = Globals::session('user_id');

        $user->delete($user_id);

        Globals::removeSession('user_id');
        Globals::removeSession('user_login');
        Globals::removeSession('user_password');
        Globals::removeSession('user_email');

        View::redirect('/');
    }


}