<?php

/**
 * Class Autoload
 *
 * Cette classe permet de charger automatiquement toutes les classes de notre projet
 * Il est inspiré d'un tutoriel disponible à cette adresse : https://www.grafikart.fr/formations/programmation-objet-php/autoload
 *
 * @author Yonel Becerra
 * @version 1.0
 */

class Autoloader {

    // On enregistre notre autoloader
    public static function register()
    {
        spl_autoload_register(array(__CLASS__, 'autoload'));
    }

    // Fonction appelée lors de l'utilisation d'une classe non déclarée
    public static function autoload($class)
    {
        $directories = [
            '..'.TLAB_DS.'kernel'.TLAB_DS,
            '..'.TLAB_DS.'app'.TLAB_DS.'controllers'.TLAB_DS,
            '..'.TLAB_DS.'app'.TLAB_DS.'controllers'.TLAB_DS.'resources'.TLAB_DS,
            '..'.TLAB_DS.'app'.TLAB_DS.'repositories'.TLAB_DS,
            '..'.TLAB_DS.'app'.TLAB_DS.'models'.TLAB_DS,
        ];

        foreach ($directories as $directory) {
            if(file_exists($directory. $class .'.php')) {
                require $directory. $class .'.php';
                continue;
            }
        }

    }
}