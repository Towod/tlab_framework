<?php

/**
 * Class Request
 * Gestion des requêtes
 *
 * @author Yonel Becerra
 */

$getRequests    = [];
$postRequests   = [];
$fileRequests   = [];

class Request {

    public static function init()
    {
        global $getRequests;
        global $postRequests;
        global $fileRequests;

        foreach ($_GET as $slug => $datas) {
            $getRequests[$slug] = htmlspecialchars($datas);
            unset($_GET[$slug]);
        }

        foreach ($_POST as $slug => $datas) {
            $postRequests[$slug] = htmlspecialchars($datas);
            unset($_POST[$slug]);
        }

        foreach ($_FILES as $slug => $datas) {
            $fileRequests[$slug] = $datas;
            unset($_FILE[$slug]);
        }
    }

    // Retourne la valeur d'un champ GET
    public static function get($slug)
    {
        global $getRequests;

        if(!empty($getRequests[$slug])) {
            return $getRequests[$slug];
        } else {
            return false;
        }
    }

    // Retourne la valeur d'un champ POST
    public static function post($slug)
    {
        global $postRequests;

        if(!empty($postRequests[$slug])) {
            return $postRequests[$slug];
        } else {
            return false;
        }
    }
}