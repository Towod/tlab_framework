<?php

/**
 * Class Route
 * Gestion des routes
 *
 * @author Yonel Becerra
 */

// Liste des routes actives
$routes = [];

class Routes {

    // Ajouter une route
    public function add($slug, $callback)
    {
        global $routes;

        // On ajoute à notre Array de route la nouvelle route
        $routes[$slug] = $callback;
    }

    // Appeler le controlleur et la fonction demandée
    public function call()
    {

        global $routes;

        $exist = false;

        foreach ($routes as $route => $callback) {
            $match = preg_match('#^'.$route.'$#', TLAB_URI, $vars);
            if($match) {

                // On récupère un tableau contenant [classToCall, functionToCall]
                $parsed = $this->parseCallBack($callback);
                // On appelle le controlleur
                $controller = new $parsed[0];
                // On lance la fonction du controlleur
                $function = $parsed[1];

                $parameters = '';

                if(!empty($vars[1])) {
                    unset($vars[0]);

                    foreach ($vars as $var) {
                        $parameters = $var.', ';
                    }

                    $parameters = preg_replace('#(, )$#', '', $parameters);
                }

                $exec = '$controller->$function($parameters);';

                eval($exec);

                $exist = true;
            }
        }

        if(!$exist) {
            View::notFound();
        } else {
            die();
        }

    }

    // Sépare le callback en [classToCall, functionToCall] à partir de 'classToCall@functionToCall'
    public function parseCallBack ($callback)
    {
        return explode('@', $callback);
    }

    // Retourne le lien vers la route demandée
    public static function linkTo($routeSlug, $e = true)
    {
        global $routes;
        $return = '';

        foreach ($routes as $route => $callback) {
            $match = preg_match('#^' . $route . '$#', TLAB_URI, $vars);
            if ($match) {


                $return = APP_URL . $routeSlug;

                if ($e)
                    echo $return;
            }
        }

        return $return;
    }

    protected function preg_array_key_exists($pattern, $array) {
        $keys = array_keys($array);
        return (int) preg_grep($pattern,$keys);
    }
}