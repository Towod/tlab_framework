<?php

/**
 * Class Request
 * Gestion des requêtes
 *
 * @author Yonel Becerra
 */

$sessions    = [];

class Globals {

    public static function init()
    {
        session_start ();

        global $sessions;

        foreach ($_SESSION as $slug => $datas) {
            $sessions[$slug] = htmlspecialchars($datas);
            $_SESSION[$slug];
        }

    }

    // Retourne la valeur d'un champ SESSION
    public static function session($slug)
    {
        global $sessions;

        if(!empty($sessions[$slug])) {
            return $sessions[$slug];
        } else {
            return false;
        }
    }

    // Retourne la valeur d'un champ SESSION
    public static function addSession($slug, $value)
    {
        global $sessions;

        $_SESSION[$slug] = $value;
        $sessions[$slug] = $value;

        return true;
    }

    // Retourne la valeur d'un champ SESSION
    public static function removeSession($slug)
    {
        global $sessions;

        unset($_SESSION[$slug]);
        unset($sessions[$slug]);

        return true;
    }
}