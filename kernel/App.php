<?php

/**
 * Class App
 * Cette classe contient toute notre application
 *
 * @author Yonel Becerra
 */

class App {

    public $defines = [];

    // Initialistation de l'application
    public function __construct()
    {
        $serverURI = explode('?', $_SERVER['REQUEST_URI'], 2);

        // Listes des constantes à définir
        $this->defines = [

            'TLAB_DS'  => DIRECTORY_SEPARATOR,
            'TLAB_URI' => $serverURI[0],

            'APP_URL'  => 'http://ecrivain.tlab',
            'APP_NAME' => 'OpenBook',
        ];
        $this->defines(); // <--- Déclaration des constantes

        // Enregistrement de l'Autoloader
        require_once 'Autoloader.php';
        Autoloader::register();

        View::add_css('bootstrap_css', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css');
        View::add_css('fa_css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
        View::add_css('header_css', Asset::css('components.header'));

        View::add_footer_js('jquery_js', 'https://code.jquery.com/jquery-3.2.1.min.js');
        View::add_footer_js('tether_js', 'https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js');
        View::add_footer_js('bootstrap_js', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js');

        // Nettoyage des requêtes
        Request::init();
        Globals::init();

        // Déclaration des routes
        $routes = new Routes();
        require_once '..'.TLAB_DS.'app'.TLAB_DS.'routes'.TLAB_DS.'web.php';

        // Lancement de la route demandée
        $routes->call();

    }

    // Défini toutes les constantes listées dans $this->defines
    protected function defines()
    {
        // Boucle sur $this->defines
        foreach ($this->defines as $constant => $result)
        {
            // Appel de la fonction define pour chaque élément du tableau
            define($constant, $result);
        }
    }

    public static function bookName()
    {
        $model = new AppModel();
        $options = $model->get(1);

        echo $options['book_name'];
    }


}