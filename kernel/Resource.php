<?php

/**
 * Class ResourceController
 * Classe mère de toutes les ressources
 *
 * @author Yonel Becerra
 */

class Resource extends Controller {

    // Nom de la ressource
    protected $slug;
    protected $model;

    public function __construct()
    {
        $modelName = $this->slug.'Model';
        $this->model = new $modelName();
    }

    // Affiche tous les éléments de la ressource
    public function index()
    {

        $resources = $this->model->all();

        if(!empty($resources)) {
            return $this->view(__FUNCTION__, [
                'page_title' => 'Billet simple pour l\'Alaska',
                'resources' => $resources
            ]);
        } else {
            return $this->view('no_result_found', [
                'page_title' => 'Billet simple pour l\'Alaska',
            ]);
        }


    }

    // Affiche seulement une ressource
    public function show($id)
    {
        $resource = $this->model->get($id);

        return $this->view(__FUNCTION__, [
            'page_title' => 'Billet simple pour l\'Alaska',
            'resource' => $resource,
        ]);
    }

    // Affiche le formulaire de création pour notre ressource
    public function create()
    {
        return $this->view(__FUNCTION__, [
            'page_title' => 'Ajouter un chapitre'
        ]);
    }

    // Sauvegarde notre ressource en base de donnée
    public function store()
    {
        $this->model->save();
        View::redirect('/');
    }

    // Affiche le formulaire d'édition de notre ressource
    public function edit($id)
    {
        $resource = $this->model->get($id);

        return $this->view(__FUNCTION__, [
            'page_title' => 'Ajouter un chapitre',
            'resource' => $resource
        ]);
    }

    // Met à jour les informations sur la ressource en BDD
    public function update($id)
    {
        $this->model->refresh($id);
        View::redirect('/'.$this->slug.'/edit/'.htmlspecialchars($id).'/');
    }

    // Détruit la ressource en BDD
    public function destroy($id)
    {
        $this->model->delete($id);
        View::redirect('/');
    }

    // Retourne la vue en fonction de la fonction
    protected function view($function, $datas = [])
    {
        $datas['resourceSlug'] = $this->slug;

        return View::get('resources.'.$this->slug.'.'.$function, $datas);
    }


}