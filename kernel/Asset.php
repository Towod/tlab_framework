<?php

/**
 * Class Asset
 * Gestion des fichiers js, css et img
 *
 * @author Yonel Becerra
 */


class Asset {


    public static function css($slug)
    {
        $slug = str_replace('.', TLAB_DS, $slug);
        $path = '..'.TLAB_DS.'app'.TLAB_DS.'assets'.TLAB_DS.'css'.TLAB_DS.$slug.'.css';

        return $path;
    }

    public static function js($slug)
    {
        $slug = str_replace('.', TLAB_DS, $slug);
        $path = '..'.TLAB_DS.'app'.TLAB_DS.'assets'.TLAB_DS.'js'.TLAB_DS.$slug.'.js';

        return $path;
    }

    public static function img($slug)
    {
        $slug = str_replace('.', TLAB_DS, $slug);
        $path = '..'.TLAB_DS.'app'.TLAB_DS.'assets'.TLAB_DS.'img'.TLAB_DS.$slug.'.jpg';

        return $path;
    }
}