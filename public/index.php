<?php
/**
 * TLab Framework - MVC framework for newbies
 *
 * @author Yonel Becerra
 * @create: 25/07/2017
 */

// Lancement de l'application
require '..'.DIRECTORY_SEPARATOR.'kernel'.DIRECTORY_SEPARATOR.'App.php';
new App();