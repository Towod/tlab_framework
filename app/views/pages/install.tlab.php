
@include('components\setup_header')

<section>
    <div class="container">
        <div class="row">
            <div class="col-6 push-sm-3 text-left">

                <div class="card mt-3">
                    <div class="card-header text-center">
                        <h1>Bienvenue sur OpenBook</h1>
                    </div>
                    <div class="card-block">



                        <div class="row">
                            <div class="col">
                                <p class="card-text text-center">Remplissez le formulaire ci-dessous pour commencer votre livre !</p>
                                <hr>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <?php Auth::registerForm(); ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <hr>
                            </div>
                        </div>

                    </div>
                </div>




            </div>
        </div>
    </div>
</section>



@include('components\setup_footer')

