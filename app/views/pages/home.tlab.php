
@include('components\header')

<section>
    <div class="container">
        <div class="row">
            <div class="col text-left">
                <h1>Hello World</h1>
                <p><?php echo $posts->title[1]; ?></p>

                <p>Bonjour {{ $name }}</p>
                <p>Je suis écrivain et souhaiterais vous faire découvrir mon roman "{{ $bookName }}" au travers de ce site.</p>
            </div>
        </div>
    </div>
</section>



@include('components\footer')

