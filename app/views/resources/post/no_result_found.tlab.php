


@include('components\header')

<section>
    <div class="container">
        <div class="row">
            <div class="col-8 push-sm-2 text-left">

                <div class="card mt-5">

                    <div class="card-header">
                        <div class="row">
                            <div class="col text-center">
                                <?php if(Auth::check()) : ?>
                                    <h2 class="mb-0"><?php App::bookName(); ?></h2>
                                <?php else : ?>
                                    <h2 class="mb-0">Ce livre est actuellement vide</h2>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <div class="card-block">
                        <div class="row text-center">
                            <div class="col-12 text-center">
                                <?php if(Auth::check()) : ?>
                                    <a href="@route('/post/create/')" class="btn btn-primary"><i class="fa fa-plus"></i> J'ajoute mon premier chapitre !</a>
                                <?php else : ?>
                                    <?php $user = Auth::userInfos() ?>
                                    <a href="mailto:<?php echo $user[1]['email']; ?>" class="btn btn-primary"><i class="fa fa-envelope"></i> Contacter l'auteur</a>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>


                </div>

                <div class="row">
                    <div class="col text-center mt-3">
                        <hr>
                        <?php if(Auth::check()) : ?>
                            <a href="@route('/logout/')" class="btn btn-warning mt-3 btn-sm"><i class="fa fa-cruz"></i> Me déconnecter</a>
                            <a href="@route('/delete_account/')" class="btn btn-danger mt-3 btn-sm"><i class="fa fa-trash"></i> Supprimer mon compte</a>
                        <?php else : ?>
                            <a href="@route('/login/')" class="btn btn-success mt-3 btn-sm"><i class="fa fa-user"></i> Me connecter</a>
                        <?php endif; ?>
                    </div>
                </div>






            </div>
        </div>
    </div>
</section>



@include('components\footer')

