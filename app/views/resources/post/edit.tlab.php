


@include('components\header')

<section>
    <div class="container">
        <div class="row">
            <div class="col-8 push-sm-2 text-left">

                <div class="row mb-4 mt-4">
                    <div class="col">
                        <a href="<?php Routes::linkTo('/'); ?>" class="btn btn-primary"><i class="fa fa-bars"></i> Retourner à l'index</a>
                    </div>
                </div>

                <div class="card">


                                <form action="<?php Routes::linkTo('/post/update/'.$resource['id'].'/'); ?>" method="POST">
                                    <div class="row">
                                        <div class="col">

                                            <div class="card-header">
                                                <div class="form-group">
                                                    <input type="text" name="title" id="title" class="form-control" value="<?php echo $resource['title']; ?>" placeholder="Titre du chapitre">
                                                </div>
                                            </div>
                                            <div class="card-block">



                                            <div class="form-group">
                                                <textarea name="content" id="content" class="form-control"><?php echo $resource['content']; ?></textarea>
                                            </div>

                                            <input type="submit" class="btn btn-success btn-block">
                                        </div>

                                    </div>
                                </form>
                            </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>



@include('components\footer')

