


@include('components\header')

<section>
    <div class="container">
        <div class="row">
            <div class="col-8 push-sm-2 text-left">

                <div class="row mt-4">

                    <div class="col-12">

                        <div class="w-100 mb-4 text-center">
                            <h2 class=""><?php App::bookName(); ?></h2>
                        </div>
                    </div>

                    <div class="col-12">
                        <article class="post card">
                            <div class="card-header text-center">
                                <h2 class="card-title mb-0"><?php echo $resource['title']; ?></h2>
                            </div>
                            <div class="card-block">

                                <p class="card-text"><?php echo htmlspecialchars_decode($resource['content']); ?></p>
                            </div>
                        </article>
                    </div>

                    <div class="col-12 mt-3 mb-4">
                        <?php
                        $next = $resource['id']+1;
                        $prev = $resource['id']-1;
                        ?>
                        <div class="btn-group w-100 mb-4">
                            <a href="<?php Routes::linkTo('/post/'.$prev.'/') ?>" class="btn btn-primary btn-block mt-2 w-25"><i class="fa fa-angle-double-left"></i> Chapitre précédent</a>
                            <a href="<?php Routes::linkTo('/'); ?>" class="btn btn-secondary btn-block w50"><i class="fa fa-bars"></i> Sommaire</a>
                            <a href="<?php Routes::linkTo('/post/'.$next.'/') ?>" class="btn btn-primary btn-block w-25">Chapitre suivant <i class="fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                </div>




            </div>
        </div>
    </div>
</section>



@include('components\footer')

