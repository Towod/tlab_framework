


@include('components\header')

<section>
    <div class="container">
        <div class="row">
            <div class="col text-left">

                <div class="row">

                        <div class="col-8 push-sm-2 mt-5">
                            <article class="card">

                                <div class="card-header text-center">
                                    <h2 class="card-title mb-0"><?php App::bookName(); ?></h2>
                                </div>

                                <div class="card-block">

                                    <div class="row">
                                        <div class="col">

                                            <?php foreach ($resources as $post) : ?>
                                                <div class="post mb-3">
                                                    <div class="row">
                                                        <div class="col-9">
                                                            <h3 class="mt-2 mb-0 ml-2"><a href="<?php Routes::linkTo('/post/'.$post['id'].'/') ?>"><?php echo $post['title']; ?></a></h3>
                                                        </div>
                                                        <div class="col-3">
                                                            <div class="text-right w-100 actions mt-2 mb-2">
                                                                <div class="mr-2">
                                                                    <a href="<?php Routes::linkTo('/post/'.$post['id'].'/') ?>" class="btn btn-success btn-sm "><i class="fa fa-eye"></i></a>
                                                                    <?php if(Auth::check()) : ?>
                                                                        <a href="<?php Routes::linkTo('/post/edit/'.$post['id'].'/') ?>" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
                                                                        <a href="<?php Routes::linkTo('/post/destroy/'.$post['id'].'/') ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                                                    <?php else : ?>
                                                                    <?php endif; ?>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>

                                        </div>
                                    </div>

                                </div>

                                <div class="card-footer text-center">

                                    <?php if(!Auth::check()) : ?>
                                    <?php else : ?>
                                        <a href="@route('/post/create/')" class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter un chapitre</a>
                                    <?php endif; ?>


                                </div>




                            </article>
                        </div>



                </div>

                <div class="row mb-4 mt-4">
                    <div class="col text-center">
                            <?php if(!Auth::check()) : ?>
                                <?php $user = Auth::userInfos() ?>
                                <a href="mailto:<?php echo $user[1]['email']; ?>" class="btn btn-primary mb-4"><i class="fa fa-envelope"></i> Contacter l'auteur du livre</a>
                                <br>
                                <a href="@route('/login/')" class="btn btn-success btn-sm"><i class="fa fa-user"></i> Me connecter</a>
                            <?php else : ?>
                                <?php Auth::logout(); ?>
                            <?php endif; ?>

                    </div>
                </div>




            </div>
        </div>
    </div>
</section>



@include('components\footer')

