@include('components\header')

<section>
    <div class="container">
        <div class="row">
            <div class="col text-left">

                <div class="row mb-4 mt-4">
                    <div class="col text-center">
                        <a href="<?php Routes::linkTo('/'); ?>" class="btn btn-primary"><i class="fa fa-bars"></i> Retourner au sommaire</a>
                    </div>
                </div>

                <div class="card">


                                <form action="@route('/post/store/')" method="POST">
                                    <div class="row">
                                        <div class="col">

                                            <div class="card-header">
                                                <div class="card-title mb-0">
                                                    <div class="form-group mb-0">
                                                        <input type="text" name="title" id="title" class="form-control" placeholder="Titre du chapitre">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-block">



                                                <div class="form-group">
                                                    <textarea name="content" id="content" class="form-control"></textarea>
                                                </div>

                                                <input type="submit" class="btn btn-success">
                                            </div>
                                        </div>
                                    </div>
                                </form>


                    </div>
                </div>

            </div>
        </div>
    </div>
</section>



@include('components\footer')

