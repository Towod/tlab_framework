@include('components\setup_header')

<section>
    <div class="container">
        <div class="row">
            <div class="col text-left">

                <div class="row">
                    <div class="col-6 push-sm-3">
                        <div class="card mt-5">

                            <div class="card-header text-center">
                                <h1>Bienvenue sur OpenBook</h1>
                            </div>

                            <div class="card-block">

                                <div class="row">
                                    <div class="col">
                                        <p class="card-text text-center">Remplissez le formulaire ci-dessous pour commencer votre livre !</p>
                                        <hr>
                                    </div>
                                </div>

                                <form action="@route('/create_user/')" method="POST">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group mb-2">
                                                <div class="input-group">
                                                    <input type="text" name="login" id="login" class="form-control" placeholder="Votre nom d'utilisateur">
                                                    <input type="password" name="password" id="password" class="form-control" placeholder="Votre mot de passe">
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="form-group mb-2">
                                                <input type="email" name="email" id="email" class="form-control" placeholder="Votre e-mail : username@ndd.com">
                                            </div>
                                            <div class="form-group mb-2">
                                                <input type="text" name="book_name" id="book_name" class="form-control" placeholder="Le titre de votre livre">
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="form-group mb-0">
                                                <input type="submit" class="btn btn-success btn-block" value="Commencer mon livre">
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
    </div>
</section>



@include('components\setup_footer')


