
@include('components\header')

<section>
    <div class="container">
        <div class="row">
            <div class="col-6 push-sm-3 text-left">



                <div class="card mt-5">
                    <div class="card-header mb-0 text-center">
                        <h2 class="card-title mb-0">Connexion</h2>
                    </div>
                    <div class="card-block">
                        <form action="<?php Routes::linkTo('/connect/'); ?>" method="POST">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group mb-2">
                                        <input type="email" name="email" id="email" class="form-control" placeholder="Votre e-mail : username@ndd.com">
                                    </div>

                                    <div class="form-group mb-2">
                                        <input type="password" name="password" id="password" class="form-control" placeholder="Votre mot de passe">
                                    </div>

                                    <input type="submit" class="btn btn-success btn-block mb-0">
                                </div>
                            </div>
                        </form>
                    </div>

                </div>

                <div class="row mb-4 mt-4">
                    <div class="col text-center">
                        <a href="<?php Routes::linkTo('/'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-bars"></i> Retourner au sommaire</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
</section>



@include('components\footer')


