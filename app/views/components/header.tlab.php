<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>{{ $page_title }}</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <?php View::css(); ?>
        <?php View::head_js(); ?>
    </head>

    <body>

        <header>
            <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                <div class="container">

                    <a class="navbar-brand mr-0" href="<?php Routes::linkTo('/'); ?>"><i class="fa fa-book fa-sm"></i> <?php echo APP_NAME; ?></a>


                </div>
            </nav>

        </header>

        <div class="content">

