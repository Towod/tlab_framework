<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>{{ $page_title }}</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <?php View::css(); ?>
        <?php View::head_js(); ?>
    </head>

    <body>


        <div class="content">

