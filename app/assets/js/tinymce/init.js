
// Prevent jQuery UI dialog from blocking focusin
jQuery(document).on('focusin', function(e) {
    if (jQuery(e.target).closest(".mce-window, .moxman-window").length) {
        e.stopImmediatePropagation();
    }
});


tinymce.init({
    selector: "textarea",  // change this value according to your HTML
    plugins: ["fullscreen", "wordcount", "autoresize"],
    menubar: false,
    branding: false,
    elementpath: false
});
