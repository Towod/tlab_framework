<?php
/**
 * Ajout des routes pour notre application
 *
 * @author Yonel Becerra
 */


// Si l'écrivrain n'est pas encore inscrit
if(!Auth::hasUser()) {
    $routes->add('/register/',                  'Auth@register');               // <--- Formulaire d'inscription
    $routes->add('/create_user/',               'Auth@register_func');          // <--- Fonction d'enregistrement de l'utilisateur
    $routes->add('/setup/',                     'InstallController@install');
}

// Si l'écrivain est connecté
if(Auth::check()) {
    $routes->add('/logout/',                    'Auth@logout_func');
    $routes->add('/delete_account/',            'Auth@delete_account');

    $routes->add('/post/create/',               'PostResource@create');
    $routes->add('/post/store/',                'PostResource@store');
    $routes->add('/post/edit/([0-9]+)/',        'PostResource@edit');
    $routes->add('/post/update/([0-9]+)/',      'PostResource@update');
    $routes->add('/post/destroy/([0-9]+)/',     'PostResource@destroy');
}

// Pour tous les visiteurs
$routes->add('/',                               'HomeController@display');
$routes->add('/login/',                         'Auth@login');
$routes->add('/connect/',                       'Auth@login_func');

$routes->add('/book/index/',                    'PostResource@index');
$routes->add('/post/([0-9]+)/',                 'PostResource@show');


