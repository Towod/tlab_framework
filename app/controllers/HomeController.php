<?php

/**
 * Class HomeController
 * Controller de la page d'accueil
 *
 * @author Yonel Becerra
 */

class HomeController extends Controller
{

    public function display()
    {
        // S'il n'y a pas encore d'utilisateur inscrit
        if(!Auth::hasUser()) {
            View::redirect('/setup/');
            die();
        } else {
            View::redirect('/book/index/');
            die();
        }
    }
}