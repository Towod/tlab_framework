<?php

/**
 * Class InstallController
 * Initialisation de l'application
 *
 * @author Yonel Becerra
 */

class InstallController extends Controller
{

    public function install()
    {
        View::redirect('/register/');
    }
}