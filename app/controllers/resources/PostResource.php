<?php

/**
 * Class PostController
 * Classe permettant le controle des articles du blog
 *
 * @author Yonel Becerra
 */
class PostResource extends Resource
{
    public function __construct()
    {
        $this->slug = 'post';

        return parent::__construct();
    }

    public function index()
    {

        View::add_css('post_index_css', Asset::css('resources.post.index'));

        return parent::index();
    }

    public function edit($id)
    {
        View::add_footer_js('tinymce_js', 'https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=t2lyvhtf78odlv7qmik1w0bigcw190qztb4aiwsyyqsqxjkz');
        View::add_footer_js('tinymce_init_js', Asset::js('tinymce.init'));

        return parent::edit($id);
    }

    public function create()
    {
        View::add_footer_js('tinymce_js', 'https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=t2lyvhtf78odlv7qmik1w0bigcw190qztb4aiwsyyqsqxjkz');
        View::add_footer_js('tinymce_init_js', Asset::js('tinymce.init'));

        return parent::create();
    }
}