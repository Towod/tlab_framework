<?php

/**
 * Class AppModel
 * Classe permettant la récupération et la sauvegarde des options de l'application
 *
 * @author Yonel Becerra
 */
$options = [
    'book_name' => '',
];

class AppModel extends Model
{
    public function __construct()
    {
        $this->table = 'app_options';
        $this->structure = ['book_name'];

        parent::__construct();
    }
}