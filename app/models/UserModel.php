<?php

/**
 * Class PostController
 * Classe permettant le controle des articles du blog
 *
 * @author Yonel Becerra
 */
class UserModel extends Model
{
    public function __construct()
    {
        $this->table = 'users';
        $this->structure = ['login', 'password', 'email'];

        parent::__construct();
    }

    public function save($datas = [])
    {
        $app = new AppModel();
        $app->update(1);

        if(empty($datas)) {
            $datas['login']     = Request::post('login');
            $datas['password']  = password_hash(Request::post('password'), PASSWORD_BCRYPT);
            $datas['email']     = Request::post('email');
        }

        $this->insert($datas);
    }
}