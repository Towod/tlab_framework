<?php

/**
 * Class PostController
 * Classe permettant le controle des articles du blog
 *
 * @author Yonel Becerra
 */
class PostModel extends Model
{
    public function __construct()
    {
        $this->table = 'posts';
        $this->structure = ['title', 'content'];

        parent::__construct();
    }
}